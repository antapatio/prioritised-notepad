import matplotlib.pyplot as plt

def generate_id():
  # Create a static variable to hold the current ID
  if not hasattr(generate_id, "counter"):
    generate_id.counter = 0

  # Increment the counter and return the current ID
  generate_id.counter += 1
  if generate_id.counter > 2000:
    generate_id.counter = 0
  return generate_id.counter

# Capture the priority, time resources, and description of the task
description = input("Enter the description of the task: ")
priority = input("Enter the priority of the task (A-B): ")
time_resources = input("Enter the time resources required for the task (1-40): ")

# Generate a unique ID for the task
task_id = generate_id()

# Save the task to a file
with open("tasks.txt", "w") as f:
  f.write(f"ID: {task_id}\n")
  f.write(f"Priority: {priority}\n")
  f.write(f"Time Resources: {time_resources}\n")
  f.write(f"Description: {description}\n")

# Create the x and y coordinates for the tasks
x = [time_resources]
y = [priority]

# Create a scatter plot of the tasks
plt.scatter(x, y)

# Add a label for each task
for i, txt in enumerate(task_id):
  plt.annotate(txt, (x[i], y[i]))

# Add labels to the x and y axes
plt.xlabel("Time Resources")
plt.ylabel("Priority")

# Show the plot
plt.show()
